package com.avios;

import java.io.Serializable;

/**
 * Product
 */
public class Product implements Serializable {

    private static final long serialVersionUID = 1L;

    private String partner;
    private String product;
    private String subproduct;

    /**
     * @return the partner
     */
    public String getPartner() {
        return partner;
    }

    /**
     * @param partner the partner to set
     */
    public void setPartner(String partner) {
        this.partner = partner;
    }

    /**
     * @return the product
     */
    public String getProduct() {
        return product;
    }

    /**
     * @param product the product to set
     */
    public void setProduct(String product) {
        this.product = product;
    }

    /**
     * @return the subproduct
     */
    public String getSubproduct() {
        return subproduct;
    }

    /**
     * @param subproduct the subproduct to set
     */
    public void setSubproduct(String subproduct) {
        this.subproduct = subproduct;
    }
}